use std::sync::mpsc::Sender;

use waef_core::{
    actors::{ActorsPlugin, UseActors},
    core::Message,
};

use super::bindings::{actor::ControlsBindingActor, cfg::ControlsCfg};

pub struct ControlsPlugin {
    cfg: ControlsCfg,
    dispatch: Sender<Message>,
}
impl ControlsPlugin {
    pub fn new(cfg: ControlsCfg, dispatch: Sender<Message>) -> Self {
        Self { cfg, dispatch }
    }
}

impl ActorsPlugin for ControlsPlugin {
    fn apply<T: UseActors>(self, target: T) -> T {
        target.use_actor(Box::new(ControlsBindingActor::new(self.cfg, self.dispatch)))
    }
}
