use serde_derive::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Default, Clone, Serialize, Deserialize)]
pub struct ControlsCfg {
    pub buttons: HashMap<String, String>,
    pub axis: HashMap<String, HashMap<String, String>>,
    pub triggers: HashMap<String, HashMap<String, String>>,
}
