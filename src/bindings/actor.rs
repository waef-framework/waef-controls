use std::{
    collections::{HashMap, HashSet},
    sync::mpsc::Sender,
};

use crate::{
    events::{ControlAxisEvent, ControlButtonEvent, ControlTriggerEvent},
    system::{
        gamepad::{GamepadAxisMoved, GamepadButtonEvent, GamepadButtonState, GamepadTriggerMoved},
        keyboard::{KeyboardEvent, KeyboardState}, mouse::{MouseButtonEvent, MouseButtonState},
    },
};
use tracing::{span, Level};
use waef_core::{
    actors::{ActorAlignment, HasActorAlignment, IsActor},
    core::{DispatchFilter, ExecutionResult, IsDispatcher, IsDisposable, IsMessage, Message},
};

use super::cfg::ControlsCfg;

type ControlsAxisBinding = (String, (f32, f32));

type ControlsTriggerBinding = (String, f32);

pub struct ControlsBindingActor {
    dispatcher: Sender<Message>,
    button_bindings: HashMap<String, Vec<String>>,
    axis_bindings: HashMap<String, Vec<ControlsAxisBinding>>,
    axis_button_states: HashMap<String, (f32, f32)>,

    trigger_bindings: HashMap<String, Vec<ControlsTriggerBinding>>,
    trigger_button_states: HashMap<String, f32>,
}
impl ControlsBindingActor {
    pub fn new(cfg: ControlsCfg, dispatcher: Sender<Message>) -> Self {
        let mut button_bindings = HashMap::<String, Vec<String>>::new();
        for (control_key, system_binding) in cfg.buttons.into_iter() {
            for system_binding in system_binding.split(',') {
                let system_binding = system_binding.to_string();
                let control_key = control_key.clone();

                if let Some(binding) = button_bindings.get_mut(&control_key) {
                    binding.push(control_key);
                } else {
                    button_bindings.insert(system_binding, vec![control_key]);
                }
            }
        }

        let mut axis_bindings = HashMap::<String, Vec<(String, (f32, f32))>>::new();
        let mut axis_button_states = HashMap::<String, (f32, f32)>::new();
        for (control, inputs) in cfg.axis {
            axis_button_states.insert(control.clone(), (0.0, 0.0));
            for (system_keys, axis) in inputs {
                for system_key in system_keys.split(',').map(|s| s.to_string()) {
                    match axis
                        .split(',')
                        .flat_map(|n| n.parse::<f32>())
                        .collect::<Vec<_>>()[..]
                    {
                        [x, y] if x != 0.0 || y != 0.0 => {
                            if let Some(bind) = axis_bindings.get_mut(&system_key) {
                                bind.push((control.clone(), (x, y)));
                            } else {
                                axis_bindings
                                    .insert(system_key.clone(), vec![(control.clone(), (x, y))]);
                            }
                        }
                        _ => (),
                    }
                }
            }
        }

        let mut trigger_bindings = HashMap::<String, Vec<(String, f32)>>::new();
        let mut trigger_button_states = HashMap::<String, f32>::new();
        for (control, inputs) in cfg.triggers {
            trigger_button_states.insert(control.clone(), 0.0);
            for (system_keys, trigger) in inputs {
                for system_key in system_keys.split(',').map(|s| s.to_string()) {
                    match trigger.parse::<f32>() {
                        Ok(delta) if delta != 0.0 => {
                            if let Some(bind) = trigger_bindings.get_mut(&system_key) {
                                bind.push((control.clone(), delta));
                            } else {
                                trigger_bindings
                                    .insert(system_key.clone(), vec![(control.clone(), delta)]);
                            }
                        }
                        _ => (),
                    }
                }
            }
        }

        ControlsBindingActor {
            dispatcher,
            button_bindings,
            axis_bindings,
            axis_button_states,
            trigger_bindings,
            trigger_button_states,
        }
    }

    fn on_button_pressed(&mut self, name: &str) {
        if let Some(button_keys) = self.button_bindings.get(name) {
            for button_key in button_keys {
                _ = self.dispatcher.send(
                    ControlButtonEvent {
                        control_name: button_key.clone(),
                        is_pressed: true,
                    }
                    .into_message(),
                )
            }
        }

        if let Some(axis_keys) = self.axis_bindings.get(name) {
            for (control_key, axis_shift) in axis_keys {
                if let Some(state) = self.axis_button_states.get_mut(control_key) {
                    state.0 += axis_shift.0;
                    state.1 += axis_shift.1;
                    let (x, y) = vector_normalize(*state);

                    _ = self.dispatcher.send(
                        ControlAxisEvent {
                            control_name: control_key.clone(),
                            x,
                            y,
                        }
                        .into_message(),
                    );
                }
            }
        }

        if let Some(trigger_keys) = self.trigger_bindings.get(name) {
            for (control_key, trigger_shift) in trigger_keys {
                if let Some(state) = self.trigger_button_states.get_mut(control_key) {
                    *state += trigger_shift;

                    _ = self.dispatcher.send(
                        ControlTriggerEvent {
                            control_name: control_key.clone(),
                            value: *state,
                        }
                        .into_message(),
                    );
                }
            }
        }
    }

    fn on_button_released(&mut self, name: &str) {
        if let Some(button_keys) = self.button_bindings.get(name) {
            for button_key in button_keys {
                _ = self.dispatcher.send(
                    ControlButtonEvent {
                        control_name: button_key.clone(),
                        is_pressed: false,
                    }
                    .into_message(),
                )
            }
        }

        if let Some(axis_keys) = self.axis_bindings.get(name) {
            for (control_key, axis_shift) in axis_keys {
                if let Some(state) = self.axis_button_states.get_mut(control_key) {
                    state.0 -= axis_shift.0;
                    state.1 -= axis_shift.1;

                    let (x, y) = vector_normalize(*state);
                    _ = self.dispatcher.send(
                        ControlAxisEvent {
                            control_name: control_key.clone(),
                            x,
                            y,
                        }
                        .into_message(),
                    );
                }
            }
        }

        if let Some(trigger_keys) = self.trigger_bindings.get(name) {
            for (control_key, trigger_shift) in trigger_keys {
                if let Some(state) = self.trigger_button_states.get_mut(control_key) {
                    *state -= trigger_shift;

                    _ = self.dispatcher.send(
                        ControlTriggerEvent {
                            control_name: control_key.clone(),
                            value: *state,
                        }
                        .into_message(),
                    );
                }
            }
        }
    }
}

fn vector_normalize((x, y): (f32, f32)) -> (f32, f32) {
    if x == 0.0 && y == 0.0 {
        (0.0, 0.0)
    } else {
        let mag = (x * x + y * y).sqrt();
        (x / mag, y / mag)
    }
}

impl std::fmt::Debug for ControlsBindingActor {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("ControlsBindingActor").finish()
    }
}

impl IsDispatcher for ControlsBindingActor {
    fn dispatch(&mut self, message: &Message) -> ExecutionResult {
        if !message.name.starts_with("controls:system") {
            ExecutionResult::NoOp
        } else {
            let _guard = span!(
                Level::TRACE,
                "controls button handler",
                message = message.name
            )
            .entered();

            let event = message.name[16..].to_string();
            if event.starts_with("gamepad:") {
                if event.starts_with("gamepad:button:") {
                    if let Some(e) = <GamepadButtonEvent>::from_message(message) {
                        match e.state {
                            GamepadButtonState::Pressed => {
                                let name = &event[..event.len() - 8];
                                self.on_button_pressed(name);
                            }
                            GamepadButtonState::Released => {
                                let name = &event[..event.len() - 9];
                                self.on_button_released(name);
                            }
                        }
                    }
                } else if event.starts_with("gamepad:axis:") {
                    if let Some(e) = <GamepadAxisMoved>::from_message(message) {
                        if let Some(axis_keys) = self.axis_bindings.get(&event) {
                            for (control_key, axis_shift) in axis_keys {
                                let (x, y) =
                                    vector_normalize((e.x * axis_shift.0, e.y * axis_shift.1));
                                _ = self.dispatcher.send(
                                    ControlAxisEvent {
                                        control_name: control_key.clone(),
                                        x,
                                        y,
                                    }
                                    .into_message(),
                                );
                            }
                        }
                    }
                } else if event.starts_with("gamepad:trigger:") {
                    if let Some(e) = <GamepadTriggerMoved>::from_message(message) {
                        if let Some(trigger_keys) = self.trigger_bindings.get(&event) {
                            for (control_key, trigger_shift) in trigger_keys {
                                let value = e.value * trigger_shift;
                                _ = self.dispatcher.send(
                                    ControlTriggerEvent {
                                        control_name: control_key.clone(),
                                        value,
                                    }
                                    .into_message(),
                                );
                            }
                        }
                    }
                }
            } else if event.starts_with("keyboard:") {
                if let Some(e) = <KeyboardEvent>::from_message(message) {
                    match e.state {
                        KeyboardState::Pressed => {
                            let name = &event[..event.len() - 8];
                            self.on_button_pressed(name);
                        }
                        KeyboardState::Released => {
                            let name = &event[..event.len() - 9];
                            self.on_button_released(name);
                        }
                    }
                }
            } else if event.starts_with("mouse:button:") {
                if let Some(e) = <MouseButtonEvent>::from_message(message) {
                    match e.state {
                        MouseButtonState::Pressed => {
                            let name = &event[..event.len() - 8];
                            self.on_button_pressed(name);
                        }
                        MouseButtonState::Released => {
                            let name = &event[..event.len() - 9];
                            self.on_button_released(name);
                        }
                    }
                }
            }
            ExecutionResult::Processed
        }
    }

    fn filter(&self) -> DispatchFilter {
        DispatchFilter::OneOf(HashSet::from(["controls:system".into()]))
    }
}
impl IsDisposable for ControlsBindingActor {
    fn dispose(&mut self) {}
}
impl HasActorAlignment for ControlsBindingActor {
    fn alignment(&self) -> ActorAlignment {
        ActorAlignment::Processing
    }
}
impl IsActor for ControlsBindingActor {
    fn weight(&self) -> u32 {
        1
    }

    fn name(&self) -> String {
        "controls".to_string()
    }
}
