use num_derive::FromPrimitive;
use waef_core::core::{IsMessage, Message};
use waef_core::{message_pod, Pod};

#[repr(u8)]
#[derive(FromPrimitive, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Pod)]
pub enum MouseButton {
    Left = 0,
    Middle = 1,
    Right = 2,
    Back = 3,
    Forward = 4,
}

impl ToString for MouseButton {
    fn to_string(&self) -> String {
        match *self {
            MouseButton::Left => "left".to_string(),
            MouseButton::Middle => "middle".to_string(),
            MouseButton::Right => "right".to_string(),
            MouseButton::Back => "back".to_string(),
            MouseButton::Forward => "forward".to_string(),
        }
    }
}

#[repr(u8)]
#[derive(FromPrimitive, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Pod)]
pub enum MouseButtonState {
    Pressed = 1,
    Released = 0,
}

#[repr(C)]
#[derive(Clone, Copy, Pod)]
pub struct MouseButtonEvent {
    pub x: u32,
    pub y: u32,

    pub button: MouseButton,
    pub state: MouseButtonState,
}

impl ToString for MouseButtonState {
    fn to_string(&self) -> String {
        match *self {
            MouseButtonState::Pressed => "pressed".to_string(),
            MouseButtonState::Released => "released".to_string(),
        }
    }
}

impl IsMessage for MouseButtonEvent {
    fn category_name() -> &'static str {
        "controls:system:mouse:button"
    }

    fn into_message(self) -> Message {
        let name = format!(
            "{}:{}:{}",
            Self::category_name(),
            self.button.to_string(),
            self.state.to_string()
        );
        Message::new_from_pod(name, self)
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }
}

#[message_pod("controls:system:mouse:move")]
pub struct MouseMoveEvent {
    pub x: u32,
    pub y: u32,
}

#[message_pod("controls:system:mouse:enter")]
pub struct MouseEnterEvent {}

#[message_pod("controls:system:mouse:leave")]
pub struct MouseLeaveEvent {}
