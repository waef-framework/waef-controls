use waef_core::message_pod;

#[message_pod("window:focus:gained")]
pub struct OnWindowFocusGained {}

#[message_pod("window:focus:lost")]
pub struct OnWindowFocusLost {}

#[message_pod("window:close_requested")]
pub struct OnWindowCloseRequested {}

#[message_pod("window:created")]
pub struct OnWindowCreated {
    pub x: u32,
    pub y: u32,
    pub fullscreen: bool,
    pub borderless: bool
}

#[message_pod("window:resized")]
pub struct OnWindowResized {
    pub x: u32,
    pub y: u32,
}
