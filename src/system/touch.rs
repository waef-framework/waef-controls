use waef_core::message_pod;

pub enum TouchPhase {
    Started,
    Moved,
    Ended,
    Cancelled,
}

#[message_pod("controls:system:touch")]
pub struct TouchEvent {
    pub finger_id: u64,
    pub force: f64,
    pub maximum_force: f64,
    pub x: u32,
    pub y: u32,
}

#[message_pod("controls:system:touch:started")]
pub struct TouchStartedEvent {
    pub finger_id: u64,
    pub force: f64,
    pub maximum_force: f64,
    pub x: u32,
    pub y: u32,
}

#[message_pod("controls:system:touch:moved")]
pub struct TouchMovedEvent {
    pub finger_id: u64,
    pub force: f64,
    pub maximum_force: f64,
    pub x: u32,
    pub y: u32,
}

#[message_pod("controls:system:touch:ended")]
pub struct TouchEndedEvent {
    pub finger_id: u64,
    pub force: f64,
    pub maximum_force: f64,
    pub x: u32,
    pub y: u32,
}

#[message_pod("controls:system:touch:cancelled")]
pub struct TouchCancelledEvent {
    pub finger_id: u64,
    pub force: f64,
    pub maximum_force: f64,
    pub x: u32,
    pub y: u32,
}
