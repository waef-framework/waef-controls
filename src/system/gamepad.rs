use num_derive::FromPrimitive;
use waef_core::{
    core::{IsMessage, Message},
    Pod,
};

#[repr(C)]
#[derive(Clone, Copy, Pod)]
pub struct GamepadConnected {
    pub gamepad_index: u32,
}

impl IsMessage for GamepadConnected {
    fn into_message(self) -> Message {
        Message::new_from_pod(
            format!("{}:{}", Self::category_name(), self.gamepad_index),
            self,
        )
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "controls:system:gamepad:connected"
    }
}

#[repr(C)]
#[derive(Clone, Copy, Pod)]
pub struct GamepadDisonnected {
    pub gamepad_index: u32,
}

impl IsMessage for GamepadDisonnected {
    fn into_message(self) -> Message {
        Message::new_from_pod(
            format!("{}:{}", Self::category_name(), self.gamepad_index),
            self,
        )
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "controls:system:gamepad:disconnected"
    }
}

#[repr(u8)]
#[derive(FromPrimitive, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Pod)]
pub enum GamepadAxis {
    Unknown = 0,
    StickLeft = 1,
    StickRight = 2,
    Dpad = 3,
    Z = 4,
}
impl ToString for GamepadAxis {
    fn to_string(&self) -> String {
        match self {
            GamepadAxis::Unknown => "unknown",
            GamepadAxis::StickLeft => "stick_left",
            GamepadAxis::StickRight => "stick_right",
            GamepadAxis::Dpad => "dpad",
            GamepadAxis::Z => "z",
        }
        .into()
    }
}

#[repr(C)]
#[derive(Clone, Copy, Pod)]
pub struct GamepadAxisMoved {
    pub gamepad_index: u32,
    pub axis: GamepadAxis,
    pub x: f32,
    pub y: f32,
}

impl IsMessage for GamepadAxisMoved {
    fn into_message(self) -> Message {
        Message::new_from_pod(
            format!(
                "{}:{}:{}",
                Self::category_name(),
                self.gamepad_index,
                self.axis.to_string()
            ),
            self,
        )
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "controls:system:gamepad:axis"
    }
}

#[repr(u8)]
#[derive(FromPrimitive, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Pod)]
pub enum GamepadTrigger {
    Unknown = 0,
    Left = 1,
    Right = 2,
    LeftAlt = 3,
    RightAlt = 4,
}
impl ToString for GamepadTrigger {
    fn to_string(&self) -> String {
        match self {
            GamepadTrigger::Unknown => "unknown",
            GamepadTrigger::Left => "left",
            GamepadTrigger::Right => "right",
            GamepadTrigger::LeftAlt => "left_alt",
            GamepadTrigger::RightAlt => "right_alt",
        }
        .into()
    }
}

#[repr(C)]
#[derive(Clone, Copy, Pod)]
pub struct GamepadTriggerMoved {
    pub gamepad_index: u32,
    pub trigger: GamepadTrigger,
    pub value: f32,
}

impl IsMessage for GamepadTriggerMoved {
    fn into_message(self) -> Message {
        Message::new_from_pod(
            format!(
                "{}:{}:{}",
                Self::category_name(),
                self.gamepad_index,
                self.trigger.to_string()
            ),
            self,
        )
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "controls:system:gamepad:trigger"
    }
}

#[repr(u8)]
#[derive(FromPrimitive, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Pod)]
pub enum GamepadButton {
    Unknown = 0,
    Start,
    Select,
    North,
    South,
    East,
    West,
    StickLeft,
    StickRight,
    ShoulderLeft,
    ShoulderRight,
    ShoulderLeftAlt,
    ShoulderRightAlt,
    DPadUp,
    DPadDown,
    DPadLeft,
    DPadRight,
    C,
    Z,
    Mode,
}
impl ToString for GamepadButton {
    fn to_string(&self) -> String {
        match self {
            GamepadButton::Unknown => "unknown",
            GamepadButton::Start => "start",
            GamepadButton::Select => "select",
            GamepadButton::North => "north",
            GamepadButton::South => "south",
            GamepadButton::East => "east",
            GamepadButton::West => "west",
            GamepadButton::StickLeft => "stick_left",
            GamepadButton::StickRight => "stick_right",
            GamepadButton::ShoulderLeft => "shoulder_left",
            GamepadButton::ShoulderRight => "shoulder_right",
            GamepadButton::ShoulderLeftAlt => "shoulder_left_alt",
            GamepadButton::ShoulderRightAlt => "shoulder_right_alt",
            GamepadButton::DPadUp => "dpad_up",
            GamepadButton::DPadDown => "dpad_down",
            GamepadButton::DPadLeft => "dpad_left",
            GamepadButton::DPadRight => "dpad_right",
            GamepadButton::C => "c",
            GamepadButton::Z => "z",
            GamepadButton::Mode => "mode",
        }
        .into()
    }
}

#[repr(u8)]
#[derive(FromPrimitive, PartialEq, Eq, PartialOrd, Ord, Clone, Copy, Pod)]
pub enum GamepadButtonState {
    Pressed = 1,
    Released = 0,
}
impl ToString for GamepadButtonState {
    fn to_string(&self) -> String {
        match *self {
            GamepadButtonState::Pressed => "pressed".to_string(),
            GamepadButtonState::Released => "released".to_string(),
        }
    }
}

#[repr(C)]
#[derive(Clone, Copy, Pod)]
pub struct GamepadButtonEvent {
    pub gamepad_index: u32,
    pub button: GamepadButton,
    pub state: GamepadButtonState,
}

impl IsMessage for GamepadButtonEvent {
    fn into_message(self) -> Message {
        Message::new_from_pod(
            format!(
                "{}:{}:{}:{}",
                Self::category_name(),
                self.gamepad_index,
                self.button.to_string(),
                self.state.to_string()
            ),
            self,
        )
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        Self::try_from_bytes(&msg.buffer)
    }

    fn category_name() -> &'static str {
        "controls:system:gamepad:button"
    }
}
