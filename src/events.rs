use waef_core::{
    core::{IsMessage, Message},
    util::pod::Pod,
};

pub struct ControlAxisEvent {
    pub control_name: String,
    pub x: f32,
    pub y: f32,
}
impl IsMessage for ControlAxisEvent {
    fn into_message(self) -> waef_core::core::Message {
        Message::new_from_pod(
            format!("{}:{}", Self::category_name(), self.control_name),
            [self.x, self.y],
        )
    }

    fn from_message(msg: &waef_core::core::Message) -> Option<Self>
    where
        Self: Sized,
    {
        let control_name = msg.name.split(':').last().unwrap_or("").to_string();
        let [x, y] = <[f32; 2]>::from_bytes(&msg.buffer);

        Some(Self { control_name, x, y })
    }

    fn category_name() -> &'static str {
        "controls:axis"
    }
}

pub struct ControlTriggerEvent {
    pub control_name: String,
    pub value: f32,
}
impl IsMessage for ControlTriggerEvent {
    fn into_message(self) -> waef_core::core::Message {
        Message::new_from_pod(
            format!("{}:{}", Self::category_name(), self.control_name),
            self.value,
        )
    }

    fn from_message(msg: &waef_core::core::Message) -> Option<Self>
    where
        Self: Sized,
    {
        let control_name = msg.name.split(':').last().unwrap_or("").to_string();

        Some(Self {
            control_name,
            value: f32::from_bytes(&msg.buffer),
        })
    }

    fn category_name() -> &'static str {
        "controls:trigger"
    }
}

pub struct ControlButtonEvent {
    pub control_name: String,
    pub is_pressed: bool,
}
impl IsMessage for ControlButtonEvent {
    fn into_message(self) -> Message {
        let message_name = format!(
            "{}:{}:{}",
            Self::category_name(),
            self.control_name,
            if self.is_pressed {
                "pressed"
            } else {
                "released"
            }
        );
        Message::new_from_pod(message_name, if self.is_pressed { 1u8 } else { 0u8 })
    }

    fn from_message(msg: &Message) -> Option<Self>
    where
        Self: Sized,
    {
        let state_key: Vec<&str> = msg.name.split(':').rev().skip(1).take(1).collect();
        let control_name = state_key[0];
        Some(Self {
            control_name: control_name.to_string(),
            is_pressed: msg.buffer[0] != 0,
        })
    }

    fn category_name() -> &'static str {
        "controls:button"
    }
}
